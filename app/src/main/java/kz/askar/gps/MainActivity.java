package kz.askar.gps;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Location last = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        Log.d("My", "Last:"+last.getLatitude()+" "+last.getLongitude()+" "+last.getAltitude()+" "
                +last.getAccuracy());

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("My", "Permissions denied");
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("My", "Network:" + location.getLatitude()+" "+location.getLongitude()+" "+
                        location.getAltitude()+" "+location.getAccuracy());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.d("My", "Network enabled");
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.d("My", "Network disabled");
            }
        });

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 5, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("My", "GPS:" + location.getLatitude()+" "+location.getLongitude()+" "+
                        location.getAltitude()+" "+location.getAccuracy());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.d("My", "GPS enabled");
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.d("My", "GPS disabled");
            }
        });
    }
}
