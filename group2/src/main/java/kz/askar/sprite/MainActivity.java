package kz.askar.sprite;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    LocationManager locationManager;
    LocationListener listener;
    Location last;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("My", location.getProvider()+":"+location.getLatitude()+" "+location.getLongitude()+" "
                        +location.getAltitude()+" "+location.getAccuracy());
                Log.d("My", "Distance:"+location.distanceTo(last));
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.d("My", provider+" status"+status);
            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.d("My", provider+" enabled");
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.d("My", provider+" disabled");
            }
        };

        locationManager = (LocationManager)
                getSystemService(LOCATION_SERVICE);
        last = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(last!=null){
            Log.d("My", "Last:"+last.getLatitude()+" "+last.getLongitude()+" "
                    +last.getAltitude()+" "+last.getAccuracy());
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("My", "Permissions denied");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }else{
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, listener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    1000, 1, listener);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode==RESULT_OK){
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d("My", "Permissions denied");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, listener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    1000, 1, listener);
        }else{
            Log.d("My", "Try again");
        }
    }
}
